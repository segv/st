# st version
VERSION = 0.8.4

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

PKG_CONFIG = pkg-config

# includes and libs
INCS = -I$(X11INC) \
       `$(PKG_CONFIG) --cflags fontconfig` \
       `$(PKG_CONFIG) --cflags freetype2` \
       `$(PKG_CONFIG) --cflags harfbuzz`
LIBS = -L$(X11LIB) -lm -lrt -lX11 -lutil -lXft -lXrender\
       `$(PKG_CONFIG) --libs fontconfig` \
       `$(PKG_CONFIG) --libs freetype2` \
       `$(PKG_CONFIG) --libs harfbuzz`

# flags
DEBUG= -g
RELEASE = -Os -march=native -mtune=native -flto -fno-semantic-interposition -fipa-pta -fdevirtualize-at-ltrans -fuse-linker-plugin -fgraphite-identity -floop-nest-optimize -fipa-pta -fno-common -fno-math-errno -fno-trapping-math -fdevirtualize-at-ltrans -fuse-linker-plugin -fomit-frame-pointer -ffloat-store -fexcess-precision=fast -ffast-math -fno-rounding-math -fno-signaling-nans -fcx-limited-range -fno-math-errno -funsafe-math-optimizations -fassociative-math -freciprocal-math -ffinite-math-only -fno-signed-zeros -fno-trapping-math -frounding-math -fsingle-precision-constant -fcx-fortran-rules
BUILDTYPE = $(RELEASE)
STCPPFLAGS = -DVERSION=\"$(VERSION)\" -D_XOPEN_SOURCE=600
STCFLAGS = $(BUILDTYPE) $(INCS) $(STCPPFLAGS) $(CPPFLAGS) $(CFLAGS)
STLDFLAGS = $(BUILDTYPE) $(LIBS) $(LDFLAGS)

# compiler and linker
# CC = c99
