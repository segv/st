# segv's terminal


### Extra stuff added
 * Compilation and optimization
   * Compiled with `-Os` and LTO.
   * `strip --strip-all dwm`.
 * Color emoji.
 * Image display.

### Dependency
 * Hack Nerd Font Mono and Noto Color Emoji: font used by `st`.

### Debugging

Change the `BUIDLTYPE` variable in `config.mk` to `$(DEBUG)`
