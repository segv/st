#ifndef ONEDARK_H
#define ONEDARK_H

static const char black[]   = "#282c34";
static const char red1[]    = "#e06c75";
static const char green[]   = "#98c379";
static const char yellow[]  = "#e5c07b";
static const char blue[]    = "#61afef";
static const char magenta[] = "#c678dd";
static const char cyan[]    = "#56b6c2";
static const char white[]   = "#abb2bf";

#endif

